class EventsController < ApplicationController
  def show
  	@content = Event.find(params[:id])
  	respond_to do |format|
			format.js
    end
  end
end
