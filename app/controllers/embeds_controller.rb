class EmbedsController < ApplicationController
  after_filter :allow_iframe, only: [:show]
  protect_from_forgery except: [:show]

  layout false

  def show
    @content = Event.find(params[:id])
  end
end