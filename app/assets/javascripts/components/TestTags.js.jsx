var TestTags = React.createClass({
	getInitialState: function() {
		return {
			tags: []
		}
	},
	handleChange: function(tags) {
		this.setState({tags: tags})
	},
	render: function() {
		return <ReactTagsInput value={this.state.tags} onChange={this.handleChange} />
	}
})