window.onload = function() {
  var scriptParam = document.getElementById('load_widget');
  var id = scriptParam.getAttribute('data-content');

  var iframe = document.createElement('iframe');

  iframe.style.backgroundColor = "transparent";
  iframe.style.verticalAlign = "text-bottom";
  iframe.style.position = "relative";
  iframe.style.width = "100%";
  iframe.style.height = "100%";
  iframe.style.minWidth = "100%";
  iframe.style.minHeight = "100%";
  iframe.style.maxWidth = "100%";
  iframe.style.maxHeight = "100%";
  iframe.style.margin = "0px";
  iframe.style.overflow = "hidden";
  iframe.style.display = "block";

  // Remove those horrible borders...
  iframe.setAttribute('frameborder', '0');
  iframe.style.border = '0';

  // Replace the iframe's src url to contain our request content
  iframe.src = scriptParam.getAttribute('src').replace(/\.js/g, '/') + id;

  var div = document.createElement('div');
  div.style.marginTop = "0px";
  div.style.marginRight = "0px";
  div.style.marginBottom = "0px";
  div.style.padding = "0px";
  div.style.border = "0px";
  div.style.background = "transparent";
  div.style.overflow = "hidden";
  div.style.position = "fixed";
  div.style.zIndex = "16000002";
  div.style.width = "227px";
  div.style.height = "103px";
  div.style.right = "10px";
  div.style.bottom = "0px";

  div.appendChild(iframe);

  document.body.appendChild(div);

};